#import "AccountProxy.h"

@implementation AccountProxy


-(id)initWithACAccount:(ACAccount*)account andPageContext:(id<TiEvaluator>)context
{
    self = [super _initWithPageContext:context];
    if (self != nil) {
        _account = account;
    }
    return self;
}

-(NSString*)userId {
    return [[_account valueForKey:@"properties"] valueForKey:@"user_id"];
}

-(NSString*)username
{
    return [_account username];
}

-(NSString*)identifier
{
    return [_account identifier];
}

-(ACAccount*)account
{
    return _account;
}

@end
