#import "TiProxy.h"
#import <Accounts/Accounts.h>

@interface AccountProxy : TiProxy
{
    ACAccount *_account;
}

-(id)initWithACAccount:(ACAccount*)account andPageContext:(id<TiEvaluator>)context;

-(NSString*)userId;
-(NSString*)username;
-(NSString*)identifier;

-(ACAccount*)account;

@end
