var Social = require('dk.napp.social');
Ti.API.info("module is => " + Social);

Ti.API.info("Twitter available: " + Social.isTwitterSupported());

// find all Twitter accounts on this phone
if(Social.isRequestTwitterSupported()){ //min iOS6 required
    var accounts = [];
    Social.addEventListener("accountList", function(e){
        Ti.API.info("Accounts:");
        accounts = e.accounts; //accounts
        accounts.forEach(function(account) {
            //NOTE the returned account now is a real Ti proxy, not a dictionary
            //so you cannot just JSON.stringify it
            Ti.API.info("Account: " + account.username + " " + account.identifier);

            Social.doReverseAuth({
                account: account,
                apiKey: "<YOUR API KEY>",
                secret: "<YOUR SECRET>"
            }, function(e) {
                Ti.API.info("reverse auth result");
                Ti.API.info(e);
            });
        });
    });

    Social.twitterAccountList();
}